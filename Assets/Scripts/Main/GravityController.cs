﻿using UnityEngine;

public interface IGravityController
{
    void ChangeGravity();
    void SetToDefault();
    void SetGravityToZero();
}

public class GravityController : IGravityController
{
    private Vector3 _defaultGravityVector = Vector3.down * 10;

    public void ChangeGravity()
    {
        Physics.gravity = -Physics.gravity;
    }

    public void SetToDefault()
    {
        Physics.gravity = _defaultGravityVector;
    }

    public void SetGravityToZero()
    {
        Physics.gravity = Vector3.zero;
    }
}
