﻿using Zenject;

public interface IGameState
{
    void StartState();
    void UpdateState(float deltaTime);
    void FinishState();
}

/// <summary>
/// Базовый класс состояний игры.
/// </summary>
public abstract class GameState : IGameState
{
    protected InputHandler _input;
    protected IVintorogGameController _gameController;

    public GameState(InputHandler input, IVintorogGameController controller)
    {
        _input = input;
        _gameController = controller;
    }
    
    public abstract void StartState();
    public abstract void UpdateState(float deltaTime);
    public abstract void FinishState();

    public class Factory : PlaceholderFactory<GameStates, GameState>
    {

    }
}
