﻿public class GameStateOver : GameState
{

    public GameStateOver(IVintorogGameController gameController, InputHandler input) : base(input, gameController)
    {
    }

    public override void StartState()
    {
    }

    public override void UpdateState(float deltaTime)
    {
        if (_input.Tap)
        {
            _gameController.SetState(GameStates.WaitingToStart);
        }
    }

    public override void FinishState()
    {
        
    }
}