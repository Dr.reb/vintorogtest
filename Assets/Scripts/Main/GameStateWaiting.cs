﻿public class GameStateWaiting : GameState
{
    private readonly ILevelController _levelManager;
    private readonly IPlayer _player;
    private readonly IGravityController _gravityController;
    private readonly IScoreSystem _scoreSystem;

    public GameStateWaiting(ILevelController manager, 
        IVintorogGameController gameController, 
        InputHandler input, 
        IPlayer player,
        IGravityController gravityController,
        IScoreSystem scoreSystem) : base(input, gameController)
    {
        _levelManager = manager;
        _player = player;
        _gravityController = gravityController;
        _scoreSystem = scoreSystem;
    }

    public override void StartState()
    {
        _gravityController.SetGravityToZero();
        _scoreSystem.ResetScore();
        _levelManager.ResetLevel();
        _player.ResetPlayer();
    }

    public override void UpdateState(float deltaTime)
    {
        if (_input.Tap)
        {
            _gameController.SetState(GameStates.Playing);
        }
    }

    public override void FinishState()
    {
        _gravityController.SetToDefault();
        _levelManager.SpawnTiles(20);
    }
}
