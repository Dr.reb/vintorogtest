﻿public class GameStatePlaying : GameState
{
    private readonly ILevelController _levelManager;
    private readonly IPlayer _player;
    private readonly IGravityController _gravityController;


    public GameStatePlaying(ILevelController manager, 
        IVintorogGameController gameController, 
        InputHandler input, 
        IPlayer player,
        IGravityController gravityController) : base(input, gameController)
    {
        _levelManager = manager;
        _player = player;
        _gravityController = gravityController;
    }

    public override void StartState()
    {
        
    }

    public override void UpdateState(float deltaTime)
    {
        _levelManager.UpdateLevel(deltaTime);

        if (_input.Tap) _gravityController.ChangeGravity();
        if (_input.Pause) _gameController.SetState(GameStates.Pause);

        if (_player.IsPlayerFell)
            _gameController.SetState(GameStates.GameOver);
    }

    public override void FinishState()
    {
    }
}
