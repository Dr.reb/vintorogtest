﻿using UnityEngine;

public class GameStatePause : GameState
{

    public GameStatePause(IVintorogGameController gameController, InputHandler input) : base(input, gameController)
    {
    }

    public override void StartState()
    {
        Time.timeScale = 0;
    }

    public override void UpdateState(float deltaTime)
    {
        if (_input.Pause)
        {
            _gameController.SetState(GameStates.Playing);
        }
    }

    public override void FinishState()
    {
        Time.timeScale = 1;
    }
}