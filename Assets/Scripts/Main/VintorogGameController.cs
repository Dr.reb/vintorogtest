﻿using System;
using Zenject;

public interface IGameStateChanged
{
    event Action<GameStates> OnStateChanged;
}

public interface IVintorogGameController
{
    void SetState(GameStates state);
}

public class VintorogGameController : IUpdatable, IInitializable, IGameStateChanged, IVintorogGameController
{
    private readonly GameState.Factory _stateFactory;

    private IGameState _currentState;

    public event Action<GameStates> OnStateChanged; 

    public VintorogGameController(GameState.Factory stateFactory)
    {
        _stateFactory = stateFactory;
    }

    public void Initialize()
    {
        SetState(GameStates.WaitingToStart);
    }

    public void SetState(GameStates state)
    {
        _currentState?.FinishState();

        _currentState = _stateFactory.Create(state);

        OnStateChanged?.Invoke(state);
        _currentState.StartState();
    }

    public void Update(float deltaTime)
    {
        _currentState.UpdateState(deltaTime);
    }
}

public enum GameStates
{
    WaitingToStart,
    Playing,
    GameOver,
    Pause
}