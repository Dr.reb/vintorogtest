﻿using UnityEngine;


public interface IMonoFactory<T> where T : MonoBehaviour
{
    T Create();
}
public class MonoFactory<T> : IMonoFactory<T> where T : MonoBehaviour
{
    protected T prefabToSpawn;
    public MonoFactory(T prefab)
    {
        prefabToSpawn = prefab;
    }

    public T Create()
    {
        return Object.Instantiate(prefabToSpawn);
    }
}