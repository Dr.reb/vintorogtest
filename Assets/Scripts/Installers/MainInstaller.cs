using System;
using UnityEngine;
using Zenject;

public class MainInstaller : MonoInstaller<MainInstaller>
{
    [SerializeField] private Prefabs _prefabs;
    
    public override void InstallBindings()
    {
        MainBindings();
        BonusBindings();
        LevelManagmentBindings();
        PlayerBindings();
        
        Container.BindInterfacesAndSelfTo<InputHandler>().AsSingle();
        Container.BindInterfacesAndSelfTo<UpdatableHandler>().AsSingle().NonLazy();

        Container.BindInstance(_prefabs.BonusPrefab);
    }

    private void BonusBindings()
    {
        Container.BindInterfacesAndSelfTo<ScoreSystem>().AsSingle().NonLazy();
        Container.BindInterfacesTo<BonusFactory>().AsSingle();
    }

    private void LevelManagmentBindings()
    {
        Container.BindInterfacesTo<LevelController>().AsSingle();
        Container.BindMemoryPool<Tile, Tile.Pool>().WithInitialSize(10).FromComponentInNewPrefab(_prefabs.TilePrefab).UnderTransformGroup("Level");
    }

    private void MainBindings()
    {
        Container.BindInterfacesAndSelfTo<VintorogGameController>().AsSingle().NonLazy();
        Container.BindInterfacesAndSelfTo<GravityController>().AsSingle();
        Container.BindFactory<GameStates, GameState, GameState.Factory>().FromFactory<GameStatesFactory>();
    }

    private void PlayerBindings()
    {
        Container.BindInterfacesAndSelfTo<PlayerConditionChecker>().AsSingle();
        Container.BindInterfacesAndSelfTo<Player>().AsSingle();
    }


    [Serializable]
    public class Prefabs
    {
        public GameObject TilePrefab;
        public BonusBehaviour BonusPrefab;
    }
}