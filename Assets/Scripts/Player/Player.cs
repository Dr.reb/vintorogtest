﻿public interface IPlayer
{
    bool IsPlayerFell { get; }
    void ResetPlayer();
}

public class Player : IPlayer
{
    private readonly IPlayerConditionChecker _condition;
    private readonly PlayerBehaviour _behaviour;

    public Player(PlayerConditionChecker conditionChecker, PlayerBehaviour behaviour)
    {
        _condition = conditionChecker;
        _behaviour = behaviour;
    }

    public bool IsPlayerFell => _condition.IsFell;

    public void ResetPlayer()
    {
        _behaviour.ResetPlayer();
    }

    public class Foo
    {
        public int i;
    }
}
