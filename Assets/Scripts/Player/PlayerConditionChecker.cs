﻿using System;
using UnityEngine;

public interface IPlayerConditionChecker
{
    bool IsFell { get; }
}

public class PlayerConditionChecker : IPlayerConditionChecker
{
    private readonly Transform _playerTransform;

    public bool IsFell
    {
        get { return Math.Abs(_playerTransform.position.y) > 5; }
    }

    public PlayerConditionChecker(PlayerBehaviour controller)
    {
        _playerTransform = controller.transform;
    }
}
