﻿using UnityEngine;

public class PlayerBehaviour : MonoBehaviour
{
    [SerializeField] private Rigidbody _rigidbody;
    private Vector3 _startPoint = Vector3.zero;
    
    public void ResetPlayer()
    {
        _rigidbody.velocity = Vector3.zero;
        transform.position = _startPoint;
    }
}
