﻿using System;

public interface IScoreSystem
{
    int Score { get; }
    void IncreaseScore();
    void ResetScore();
}

/// <summary>
/// Система подсчёта очков
/// </summary>
public class ScoreSystem : IScoreSystem
{
    private int _currentScore;

    public int Score
    {
        get { return _currentScore; }
        private set
        {
            _currentScore = value;
            ScoreChanged?.Invoke(value);
        }
    }

    public event Action<int> ScoreChanged;


    public void IncreaseScore()
    {
        Score++;
    }

    public void ResetScore()
    {
        Score = 0;
    }
}
