﻿using UnityEngine;
using UnityEngine.UI;

public class GameOverStateWindowView : WindowView
{
    [SerializeField] private Button _restartButton;

    private void Awake()
    {
        _restartButton.onClick.AddListener(() => InputHandler.Tap = true);
    }
}