﻿using UnityEngine;
using Zenject;

public class WindowView : MonoBehaviour
{
    [SerializeField] private GameStates _state;

    protected InputHandler InputHandler;
    
    public GameStates States => _state;

    [Inject]
    private void Construct(InputHandler inputHandler)
    {
        InputHandler = inputHandler;
    }
}