using System;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

public class WindowsController : MonoBehaviour
{
    [SerializeField] private List<WindowView> _views;
    private Dictionary<GameStates, WindowView> _cachedViews;
    private WindowView _currentView;
    
    [Inject]
    private void Construct(IGameStateChanged gameStateChanged)
    {
        _cachedViews = new Dictionary<GameStates, WindowView>(_views.Count);
        foreach (var windowView in _views)
        {
            _cachedViews.Add(windowView.States, windowView);
            windowView.gameObject.SetActive(false);
        }

        gameStateChanged.OnStateChanged += OnStateChanged;
    }

    private void OnStateChanged(GameStates newState)
    {
        if (_currentView != null) _currentView.gameObject.SetActive(false);
        if (!_cachedViews.ContainsKey(newState))
            throw new Exception($"В контроллере окон нет окна для состояния {newState}");
        _currentView = _cachedViews[newState];
        _currentView.gameObject.SetActive(true);
    }
}