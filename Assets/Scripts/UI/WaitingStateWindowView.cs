﻿using UnityEngine;
using UnityEngine.UI;

public class WaitingStateWindowView : WindowView
{
    [SerializeField] private Button _startButton;

    private void Awake()
    {
        _startButton.onClick.AddListener(() => InputHandler.Tap = true);
    }
}