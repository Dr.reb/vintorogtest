﻿using TMPro;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

public class GameplayStateWindowView : WindowView
{
    [SerializeField] private Button _tapButton;
    [SerializeField] private Button _pauseButton;
    [SerializeField] private TextMeshProUGUI _scoreText;

    private IScoreSystem _scoreSystem;

    [Inject]
    private void Construct(IScoreSystem scoreSystem)
    {
        _scoreSystem = scoreSystem;
    }

    private void Update()
    {
        if (_scoreSystem == null) return;
        _scoreText.text = _scoreSystem.Score.ToString();
    }

    private void OnScoreChanged(int score)
    {
        _scoreText.text = score.ToString();
    }

    private void Awake()
    {
        _tapButton.onClick.AddListener(() => InputHandler.Tap = true);
        _pauseButton.onClick.AddListener(() => InputHandler.Pause = true);
    }
}