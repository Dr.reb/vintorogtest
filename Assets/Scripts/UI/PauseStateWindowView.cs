﻿using UnityEngine;
using UnityEngine.UI;

public class PauseStateWindowView : WindowView
{
    [SerializeField] private Button _resumeButton;
    
    
    private void Awake()
    {
        _resumeButton.onClick.AddListener(() => InputHandler.Pause = true);
    }
}