﻿public class InputHandler : ILateUpdatable
{
    public bool Tap
    {
        get;
        set;
    }

    public bool Pause
    {
        get;
        set;
    }

    public void LateUpdate(float deltaTime)
    {
        Tap = false;
        Pause = false;
    }
}
