using System.Collections;
using System.Collections.Generic;

public class TestLinkedList<T> : IEnumerable<T>
{
    private TestLinkedListItem<T> _first;
    private TestLinkedListItem<T> _last;
    private int _length = 0;

    public int Length => _length;
    
    public void Add(T item)
    {
        var listItem = new TestLinkedListItem<T>(item);
        if (_first == null)
            _first = listItem;
        else
            _last.Next = listItem;

        _last = listItem;
        _length++;
    }

    public void DeleteFirstItem()
    {
        if (_length == 0) return;
        if (_length == 1)
        {
            _first = null;
            _last = null;
            _length--;
            return;
        }

        _first = _first.Next;
        _length--;
    }
    
    public IEnumerator<T> GetEnumerator()
    {
        var current = _first;
        for (int i = 0; i < _length; i++)
        {
            yield return current.Item;
            current = current.Next;
        }
    }

    IEnumerator IEnumerable.GetEnumerator()
    {
        return GetEnumerator();
    }
}

public class TestLinkedListItem<T>
{
    public T Item { get; set; }
    public TestLinkedListItem<T> Next { get; set; }

    public TestLinkedListItem(T item)
    {
        Item = item;
    }
}
