﻿using System;
using UnityEngine;

public class UpdateHolder : MonoBehaviour
{
    private Action<float> _onUpdateAction;
    private Action<float> _onLateUpdateAction;
    public void Init(Action<float> onUpdateAction, Action<float> onLateUpdateAction)
    {
        _onUpdateAction = onUpdateAction;
        _onLateUpdateAction = onLateUpdateAction;
    }

    private void Update()
    {
        _onUpdateAction?.Invoke(Time.deltaTime);
    }

    private void LateUpdate()
    {
        _onLateUpdateAction?.Invoke(Time.deltaTime);
    }
}