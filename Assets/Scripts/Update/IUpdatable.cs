﻿public interface IUpdatable
{
    void Update(float deltaTime);
}

public interface ILateUpdatable
{
    void LateUpdate(float deltaTime);
}