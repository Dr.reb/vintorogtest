﻿using System.Collections.Generic;
using UnityEngine;

public class UpdatableHandler
{
    private List<IUpdatable> _updatables;
    private List<ILateUpdatable> _lateUpdatables;
    public UpdatableHandler(List<IUpdatable> updatables, List<ILateUpdatable> lateUpdatables)
    {
        _updatables = updatables;
        _lateUpdatables = lateUpdatables;
        new GameObject("UpdateHolder").AddComponent<UpdateHolder>().Init(OnUpdate, OnLateUpdate);
    }

    private void OnUpdate(float deltaTime)
    {
        foreach (var updatable in _updatables)
        {
            updatable.Update(deltaTime);
        }
    }
    private void OnLateUpdate(float deltaTime)
    {
        foreach (var updatable in _lateUpdatables)
        {
            updatable.LateUpdate(deltaTime);
        }
    }
}