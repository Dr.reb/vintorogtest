﻿using UnityEngine;
using Random = System.Random;

public interface ILevelController
{
    void SpawnTiles(int number);
    void ResetLevel();
    void UpdateLevel(float deltaTime);
}

public class LevelController : ILevelController
{
    private readonly IMonoFactory<BonusBehaviour> _bonusFactory;
    private readonly Tile.Pool _pool;
    private readonly IScoreSystem _scoreSystem;
    private readonly Random rnd;

    private float _speed = 5;
    private float _currentPosition = 0;
    private float _pointOfDestruction = -30;
    public LevelController(IMonoFactory<BonusBehaviour> bonusFactory, Tile.Pool pool, IScoreSystem scoreSystem)
    {
        _bonusFactory = bonusFactory;
        _pool = pool;
        _scoreSystem = scoreSystem;
        rnd = new Random();
    }

    public void SpawnTiles(int number)
    {
        for (int i = 0; i < number; i++)
        {
            SpawnTile(i == 0);
        }
    }

    public void ResetLevel()
    {
        _currentPosition = 0;
        _pool.DespawnAll();
    }

    public void UpdateLevel(float deltaTime)
    {
        if (_pool.NumActive < _pool.NumTotal)
            SpawnTile();

        MoveTiles(deltaTime);
        CheckTiles();
    }

    private void CheckTiles()
    {
        if (_pool.SpawnedList[0].transform.position.z < _pointOfDestruction) _pool.Despawn(_pool.SpawnedList[0]);
    }
    
    
    private void MoveTiles(float deltaTime)
    {
        foreach (var gameObject in _pool.SpawnedList)
        {
            gameObject.transform.position += Vector3.back * deltaTime * _speed;
        }

        _currentPosition -= deltaTime * _speed;
    }

    private void SpawnTile(bool startTile = false)
    {
        Tile tile = _pool.Spawn();
        tile.transform.position = Vector3.forward * _currentPosition;
        _currentPosition += 10;

        if(!startTile)
        {
            switch (rnd.Next(0, 3))
            {
                case 0: 
                    tile.RemoveLowerTile();
                    SpawnBonus(tile);
                    break;
                case 1:                    
                    tile.RemoveUpperTile();
                    SpawnBonus(tile);
                    break;
            }

        }
    }

    private void SpawnBonus(Tile tile)
    {
        BonusBehaviour bonus = _bonusFactory.Create();
        bonus.OnCollisionWithPlayer += _scoreSystem.IncreaseScore;
        tile.SetBonus(bonus.gameObject);
    }

}
