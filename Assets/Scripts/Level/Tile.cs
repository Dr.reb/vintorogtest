﻿using UnityEngine;
using System.Collections.Generic;
using Zenject;

public class Tile : MonoBehaviour
{
    [SerializeField] private GameObject _upperTile;
    [SerializeField] private GameObject _lowerTile;
    [SerializeField] private GameObject _bonus;

    public void RemoveUpperTile()
    {
        _upperTile.SetActive(false);
    }
    public void RemoveLowerTile()
    {
        _lowerTile.SetActive(false);
    }

    public void SetBonus(GameObject bonus)
    {
        _bonus = bonus;
        _bonus.transform.parent = transform;
        _bonus.transform.localPosition = Vector3.zero;
    }
    private void OnDespawn()
    {
        _upperTile.SetActive(true);
        _lowerTile.SetActive(true);
        if (_bonus != null) Destroy(_bonus);
    }
    
    public class Pool : MonoMemoryPool<Tile>
    {
        private List<Tile> _spawnedList = new(100);

        public IReadOnlyList<Tile> SpawnedList => _spawnedList;

        protected override void OnSpawned(Tile item)
        {
            _spawnedList.Add(item);
            base.OnSpawned(item);
        }

        protected override void OnDespawned(Tile item)
        {
            item.OnDespawn();
            _spawnedList.Remove(item);
            base.OnDespawned(item);
        }

        public void DespawnAll()
        {
            while(_spawnedList.Count != 0)
            {
                Despawn(_spawnedList[0]);
            }
        }
    }
}



