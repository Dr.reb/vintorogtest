﻿using System;
using UnityEngine;

public class BonusBehaviour : MonoBehaviour
{
    public event Action OnCollisionWithPlayer;
    protected virtual void OnTriggerEnter(Collider other)
    {
        if(other.CompareTag("Player"))
        {
            OnCollisionWithPlayer?.Invoke();
            OnCollisionWithPlayer = null;
            Destroy(gameObject);
        }
    }
}
